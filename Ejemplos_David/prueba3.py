#Sniffer con scapy

#! /usr/bin/env python3

from collections import Counter
from scapy.all import sniff

## Contador de paquetes
packet_counts = Counter()


## Accion a hacer cuando reciba un paquete
def custom_action(packet):
    # Crear una tupla de Src/Dst ordenada
    key = tuple(sorted([packet[0][1].src, packet[0][1].dst]))
    packet_counts.update([key])
    return f"Packet #{sum(packet_counts.values())}: {packet[0][1].src} ==> {packet[0][1].dst}"


## Setup sniff, filtering for IP traffic
sniff(filter="ip", prn=custom_action, count=10)

## Print out packet count per A <--> Z address pair
print("\n".join(f"{f'{key[0]} <--> {key[1]}'}: {count}" for key, count in packet_counts.items()))